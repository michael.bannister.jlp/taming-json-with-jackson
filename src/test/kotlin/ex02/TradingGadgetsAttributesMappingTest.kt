@file:Suppress("MemberVisibilityCanBePrivate", "SpellCheckingInspection")

package ex02

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.containsExactlyInAnyOrder
import strikt.assertions.isEqualTo
import java.util.*

class TradingGadgetsAttributesMappingTest {

    data class Product(
        val id: UUID,
        val name: String,
        val attributes: List<Attribute>,
    )

    data class Attribute(
        val name: String,
        val value: String,
    )

    val productJson = """
        {
            "id": "4686dda6-c3c6-4d0f-8ab2-279234aa079b",
            "name": "LEGO 71043 Harry Potter Hogwarts Castle",
            "attributes": [
                { "name": "stockItemId", "value": "a4b3c2d1" },
                { "name": "weight", "value": "5kg" }
            ]
        }
    """.trimIndent()

    @Test
    fun `deserialize product with string attributes`() {
        val objectMapper = jacksonObjectMapper()

        val product = objectMapper.readValue<Product>(productJson)

        expectThat(product) {
            get { id } isEqualTo UUID.fromString("4686dda6-c3c6-4d0f-8ab2-279234aa079b")
            get { attributes }.containsExactlyInAnyOrder(
                Attribute("stockItemId", "a4b3c2d1"),
                Attribute("weight", "5kg"),
            )
        }
    }

}

