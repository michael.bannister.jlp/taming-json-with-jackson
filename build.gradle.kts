plugins {
    kotlin("jvm") version "1.4.21"
}

group = "uk.teadd"
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
}

dependencies {
    implementation(platform("com.fasterxml.jackson:jackson-bom:2.12.0"))
    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    testImplementation(platform("org.junit:junit-bom:5.7.0"))
    testImplementation("io.strikt:strikt-core:0.28.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("FAILED", "SKIPPED")
        exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        jvmTarget = "11"
    }
}
